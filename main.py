from model.hypothese import Hypothese
from model.systeme import Systeme
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("lecture")
args = parser.parse_args()


def remove_useless_whitespace(string):
    while string[0]== " ":
        string = string[1:]
    while string[-1]== " ":
        string = string[:-1]
    return string

def custom_readline(file):
    line = file.readline()
    if line[-1]=="\n":
        line = line[:-1]
    return line

#list d'hypothese & resultats
hypothese_list = []
resultats = []
# Creation du systeme d'équation
s1 = Systeme()


try:
    with open(args.lecture, "r+") as file:
        file.seek(0, 2)
        EndLocation = file.tell()
        file.seek(0)
        lachaine = custom_readline(file)
        if lachaine == "#Hypothese" or lachaine == "#hypothese":
            lachaine = custom_readline(file)
            while lachaine != "#equations" and lachaine != "#Equation":
                lachaine = remove_useless_whitespace(lachaine)
                hypothese_list.append(Hypothese(lachaine))
                # print("L'hypothèse est la suivante: ", lachaine)
                lachaine = custom_readline(file)
        while file.tell() != EndLocation:
            lachaine = custom_readline(file)
            composant = lachaine.split("=>")
            premisses = composant[0].split("and")
            for i in range(0, len(premisses)):
                premisses[i] = remove_useless_whitespace(premisses[i])
            composant[1] = remove_useless_whitespace(composant[1])
            s1.ajout_equation(premisses, composant[1])
            # print("L'equation est la suivante :", lachaine)
        file.close()
except FileNotFoundError:
    print("Fichier non trouvé, vérifiez le nom!")
    exit(1)

# Moteur d'inferences
print('Hypotheses : ')
for h in hypothese_list:
    h.affiche_hypothese()
print('Systeme :')
s1.affiche_systeme()

for h in hypothese_list:
    e_index = 0
    while e_index < len(s1.get_list_equation()):
        e=s1.get_list_equation()[e_index]
        for p in e.get_list_hypothese():
            if (h.equals(p)):
                 e.remove_hypothese(p)
        if (e.hypothese_vide()):
            resultat = e.get_resultat()
            hypothese_list.append(resultat)
            resultats.append(resultat)
            s1.remove_equation(e)
            e_index-=1
        e_index += 1
    print('Systeme après traitement de l\'hypothese:', h.get_hypothese())
    s1.affiche_systeme()
print('Conclusions :')
for r in resultats:
    r.affiche_hypothese()