class Hypothese:

    # Constructeur de la classe.
    # Paramètre description_hypothese : string -> l'hypothese
    # Attribut description_hypothese : string -> hypothese
    def __init__(self, description_hypothese):
        self.description_hypothese = description_hypothese

    # Getter d'attribut
    # Retourne : string -> l'hypothese
    def get_hypothese(self):
        return self.description_hypothese

    # Comparaison entre 2 hypothese
    # Retourne : booléen -> true si 2 hypotheses équivalente, false sinon
    def equals(self, hypothese):
        return hypothese.description_hypothese == self.description_hypothese

    # Affichage de l'hypothese en console
    def affiche_hypothese(self):
        print(self.get_hypothese())