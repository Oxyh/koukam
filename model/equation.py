from model.hypothese import Hypothese


class Equation:

    # Constructeur de la classe.
    # Attribut list_hypothese : Hypothese[] -> liste des hypothese de l'équation
    # Attribut resultat : Hypothese -> conclusion de l'équation
    def __init__(self):
        self.list_hypothese =  []
        self.resultat = None

    # Getter d'attribut
    # Retourne : Hypothese[] -> liste des hypothese de l'équation
    def get_list_hypothese(self):
        return self.list_hypothese

    # Getter d'attribut
    # Retourne : Hypothese -> conclusion de l'équation
    def get_resultat(self):
        return self.resultat

    # Setter d'attribut : ajoute une hypothese à l'attribut list_hypothese
    # Récupère l'hypothese sous forme de chaine de caractère et crée l'objet Hypothese
    # Paramètre hypothese : string -> description de l'hypothese
    def ajout_hypothese(self, hypothese):
        self.list_hypothese.append(Hypothese(hypothese))

    # Setter d'attribut : ajoute une conclusion à l'objet
    # Récupère la conclusion sous forme de chaine de caractère et crée l'objet Hypothese
    # Paramètre resultat : string -> description de la conclusion
    def ajout_resultat(self, resultat):
        self.resultat = Hypothese(resultat)

    # Supprime une hypothese de l'attribut list_hypothese
    # Paramètre : Hypothese -> hypothese à supprimer
    def remove_hypothese(self, hypothese):
        self.list_hypothese.remove(hypothese)

    # Controle de vérification d'hypothese
    # Si toutes les hypotheses de l'équation sont remplie (ie liste vide), l'équation est vérifiée
    # Retourne : booléen -> true si la liste est vide, false sinon
    def hypothese_vide(self):
        return len(self.list_hypothese)==0

    # Affiche l'équation en console
    def affiche_equation(self):
        equation = ""
        for h in self.list_hypothese:
            equation+= h.get_hypothese()
            if (not h.equals(self.list_hypothese[-1])):
                equation += " AND "
        equation += " => " + self.resultat.get_hypothese()
        print(equation)

