from model.equation import Equation


class Systeme:

    # Constructeur de la classe.
    # Attribut list_equation : Equation[] -> liste des equation du systeme
    def __init__(self):
        self.list_equation = []

    # Getter d'attribut
    # Retourne : Equation[] -> liste des équation du systeme
    def get_list_equation(self):
        return self.list_equation

    # Setter d'attribut
    # Ajoute une équation à la liste list_equation
    # Création de l'objet equation à partir d'une liste d'hypothese et d'une conclusion
    # Parametre list_hypothese : string[] -> liste d'hypothese de l'équation
    # Parametre conclusion : string -> conclusion de l'équation
    def ajout_equation(self, list_hypothese, conclusion):
        equation = Equation()
        for h in list_hypothese:
            equation.ajout_hypothese(h)
        equation.ajout_resultat(conclusion)
        self.list_equation.append(equation)

    # Supprime une equation de l'attribut list_equation
    # Si une équation à déjà été vérifiée, on la supprime pour éviter de la re-traiter
    # Paramètre : Equaton -> equation à supprimer
    def remove_equation(self, equation):
        self.list_equation.remove(equation)

    # Affichage des equations du systeme en console
    def affiche_systeme(self):
        for e in self.list_equation:
            e.affiche_equation()



